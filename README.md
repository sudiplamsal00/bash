# bash


Date:

Attendees:

Agenda:

  ----- ---------------------------------------------------
  1\.   Prepare To Do list for June
  2\.   New team structure for the month
  3\.   Project for the month of June
  4\.   Performance development planning for the DevOps
  5\.   Performance development planning for the Products
  6\.   Induction of New CSE
  ----- ---------------------------------------------------

Discussion:

  --------------------- ------------ ------------
  Agenda topic          Discussion   Conclusion
  To do list for June                
  Team Structure                     
  Projects                           
  PDP for DevOps                     
  PDP for Products                   
  Induction                          
  --------------------- ------------ ------------

Teams Retrospective:

Closure of Meeting:

The meeting was closed with thanks to all participants and a reminder
that end of monthly meeting will be held on 30th june 2020 at 5:15 pm
(Nepal time).